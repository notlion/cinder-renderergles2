#include "RendererGLES.h"

#include "cinder/ip/Flip.h"

#import "AppImplCocoaTouchRendererGLES.h"

namespace cinder { namespace app {

RendererGLES::RendererGLES(ApiVersion aApiVersion, int aAntiAliasing)
: Renderer{},
  mImpl{0},
  mApiVersion{aApiVersion},
  mAntiAliasing{aAntiAliasing}
{
}

const int RendererGLES::sAntiAliasingSamples[] = { 0, 2, 4, 6, 8, 16, 32 };

void RendererGLES::setAntiAliasing(int aAntiAliasing) {
  mAntiAliasing = aAntiAliasing;
}

void RendererGLES::setup(App *aApp,
                         const Area &frame,
                         UIView *cinderView,
                         RendererRef sharedRenderer) {
  mApp = aApp;

  mImpl = [[AppImplCocoaTouchRendererGLES alloc] initWithFrame:cocoa::createCgRect(frame)
                                                    cinderView:(UIView*)cinderView
                                                           app:mApp
                                                      renderer:this
                                                sharedRenderer:std::dynamic_pointer_cast<RendererGLES>(sharedRenderer)];
}

EAGLContext* RendererGLES::getEaglContext() const {
  return [mImpl getEaglContext];
}

void RendererGLES::startDraw() {
  [mImpl makeCurrentContext];
}

void RendererGLES::finishDraw() {
  [mImpl flushBuffer];
}

void RendererGLES::setFrameSize(int width, int height) {
  [mImpl setFrameSize:CGSizeMake(width, height)];
}

void RendererGLES::defaultResize() {
  [mImpl defaultResize];
}

void RendererGLES::makeCurrentContext() {
  [mImpl makeCurrentContext];
}

Surface RendererGLES::copyWindowSurface(const Area &area) {
  Surface s(area.getWidth(), area.getHeight(), true);

  glFlush(); // there is some disagreement about whether this is necessary, but ideally performance-conscious users will use FBOs anyway

  GLint oldPackAlignment;
  glGetIntegerv(GL_PACK_ALIGNMENT, &oldPackAlignment);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);

  GLint x = area.x1;
  GLint y = mApp->getWindow()->toPixels(mApp->getWindowHeight()) - area.y2;
  GLint w = area.getWidth();
  GLint h = area.getHeight();
  glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, s.getData());

  glPixelStorei(GL_PACK_ALIGNMENT, oldPackAlignment);
  ip::flipVertical(&s);

  return s;
}

}} // namespace cinder::app
