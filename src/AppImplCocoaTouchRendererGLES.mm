#import "AppImplCocoaTouchRendererGLES.h"

#import "gles.h"

@interface AppImplCocoaTouchRendererGLES()

- (void)layoutSubviews;
- (void)allocateGraphics:(ci::app::RendererGLESRef)sharedRenderer;

@end

@implementation AppImplCocoaTouchRendererGLES

- (id)initWithFrame:(CGRect)frame
         cinderView:(UIView*)cinderView
                app:(ci::app::App*)app
           renderer:(ci::app::RendererGLES*)renderer
     sharedRenderer:(ci::app::RendererGLESRef)sharedRenderer
{
  mCinderView = cinderView;
  mApp = app;

  CAEAGLLayer *eaglLayer = (CAEAGLLayer*)cinderView.layer;
  eaglLayer.opaque = TRUE;
  eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,
                                  kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,
                                  nil];

  if (renderer->getApiVersion() == ci::app::RendererGLES::API_GLES3) {
    mApiVersion = kEAGLRenderingAPIOpenGLES3;
  }
  else if (renderer->getApiVersion() == ci::app::RendererGLES::API_GLES2) {
    mApiVersion = kEAGLRenderingAPIOpenGLES2;
  }

  mBackingWidth = 0;
  mBackingHeight = 0;

  mPointsWidth = 0;
  mPointsHeight = 0;

  mMsaaSamples = ci::app::RendererGLES::sAntiAliasingSamples[renderer->getAntiAliasing()];
  mUsingMsaa = mMsaaSamples > 0;

  [self allocateGraphics:sharedRenderer];

  return self;
}

- (void)allocateGraphics:(ci::app::RendererGLESRef)sharedRenderer
{
  if (sharedRenderer) {
    EAGLSharegroup *sharegroup = sharedRenderer->getEaglContext().sharegroup;
    mContext = [[EAGLContext alloc] initWithAPI:mApiVersion sharegroup:sharegroup];
  }
  else {
    mContext = [[EAGLContext alloc] initWithAPI:mApiVersion];
  }

  if (!mContext || ![EAGLContext setCurrentContext:mContext]) {
//    [self release];
    return;
  }

  // Create default framebuffer object. The backing will be allocated for the current layer in -resizeFromLayer
  glGenFramebuffers(1, &mViewFramebuffer);
  glGenRenderbuffers(1, &mViewRenderBuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, mViewFramebuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, mViewRenderBuffer);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mViewRenderBuffer);

  if (mUsingMsaa) {
    glGenFramebuffers(1, &mMsaaFramebuffer);
    glGenRenderbuffers(1, &mMsaaRenderBuffer);

    glBindFramebuffer(GL_FRAMEBUFFER, mMsaaFramebuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mMsaaRenderBuffer);

    glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, mMsaaSamples, GL_RGB5_A1, 0, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mMsaaRenderBuffer);

    glGenRenderbuffers(1, &mDepthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthRenderBuffer);
    glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, mMsaaSamples, GL_DEPTH_COMPONENT16, 0, 0 );
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthRenderBuffer);
  }
  else {
    glGenRenderbuffers(1, &mDepthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, 0, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthRenderBuffer);

    glGenRenderbuffers(1, &mDepthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, 0, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthRenderBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, mDepthRenderBuffer);
  }
}

- (EAGLContext*)getEaglContext
{
  return mContext;
}

- (void)layoutSubviews
{
  [EAGLContext setCurrentContext:mContext];

  // Allocate color buffer backing based on the current layer size
  glBindFramebuffer(GL_FRAMEBUFFER, mViewFramebuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, mViewRenderBuffer);

  [mContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)mCinderView.layer];

  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &mBackingWidth);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &mBackingHeight);

  if (mUsingMsaa) {
    glBindFramebuffer(GL_FRAMEBUFFER, mMsaaFramebuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthRenderBuffer);
    glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, mMsaaSamples, GL_DEPTH_COMPONENT16, mBackingWidth, mBackingHeight);
    glBindRenderbuffer(GL_RENDERBUFFER, mMsaaRenderBuffer);
    glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, mMsaaSamples, GL_RGB5_A1, mBackingWidth, mBackingHeight);
  }
  else {
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, mBackingWidth, mBackingHeight);
  }

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
  }
}

- (void)makeCurrentContext
{
  [EAGLContext setCurrentContext:mContext];

  // This application only creates a single default framebuffer which is already bound at this point.
  // This call is redundant, but needed if dealing with multiple framebuffers.
  if (mUsingMsaa) {
    glBindFramebuffer(GL_FRAMEBUFFER, mMsaaFramebuffer);
  }
  else {
    glBindFramebuffer(GL_FRAMEBUFFER, mViewFramebuffer);
  }

  glViewport(0, 0, mBackingWidth, mBackingHeight);

}

- (void)flushBuffer
{
  if (mUsingMsaa) {
    GLenum attachments[] = { GL_DEPTH_ATTACHMENT };
    glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, attachments);

    glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, mMsaaFramebuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, mViewFramebuffer);

    glResolveMultisampleFramebufferAPPLE();
  }

  glBindRenderbuffer(GL_RENDERBUFFER, mViewRenderBuffer);
  [mContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)setFrameSize:(CGSize)newSize
{
  [self layoutSubviews];
}

- (void)defaultResize
{
  glViewport(0, 0, mBackingWidth, mBackingHeight);
}

- (BOOL)needsDrawRect
{
  return NO;
}

@end
