#include "gles.h"

using namespace std;
using namespace ci;

namespace gles {

static string getInfoLog(GLuint handle,
                         function<void(GLuint, GLenum, GLint*)> glGetXXiv,
                         function<void(GLuint, GLsizei, GLsizei*, GLchar*)> glGetXXInfoLog)
{
  GLint logLength;
  glGetXXiv(handle, GL_INFO_LOG_LENGTH, &logLength);

  char* log = reinterpret_cast<char*>(malloc(logLength));
  glGetXXInfoLog(handle, logLength, NULL, log);
  auto logStr = string(log, logLength);
  free(log);
  
  return logStr;
}


TexturePtr createTexture() {
  GLuint handle;
  glGenTextures(1, &handle);
  return TexturePtr{handle};
}

TexturePtr allocTexture2d(GLsizei width, GLsizei height, TexturePixelFormat fmt) {
  auto texture = createTexture();
  bindTexture2d(texture);
  glTexImage2D(GL_TEXTURE_2D, 0, fmt.internalFormat, width, height, 0, fmt.format, fmt.type, nullptr);
  unbindTexture2d();
  return texture;
}

void bindTexture2d(const TexturePtr& texture) {
  glBindTexture(GL_TEXTURE_2D, texture.get());
}
void bindTexture2d(const TexturePtr& texture, GLenum unit) {
  glActiveTexture(unit);
  bindTexture2d(texture);
}
void unbindTexture2d() {
  glBindTexture(GL_TEXTURE_2D, NULL);
}

void setTextureFilterMin(GLint filter) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
}
void setTextureFilterMag(GLint filter) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
}
void setTextureFilter(GLint filter) {
  setTextureFilterMin(filter);
  setTextureFilterMag(filter);
}

void setTextureWrapS(GLint wrap) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
}
void setTextureWrapT(GLint wrap) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
}
void setTextureWrap(GLint wrap) {
  setTextureWrapS(wrap);
  setTextureWrapT(wrap);
}


FramebufferPtr createFramebuffer() {
  GLuint handle;
  glGenFramebuffers(1, &handle);
  return FramebufferPtr{handle};
}

void bindFramebuffer(const FramebufferPtr& framebuffer) {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.get());
}
void unbindFramebuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, NULL);
}


RenderbufferPtr createRenderbuffer() {
  GLuint handle;
  glGenRenderbuffers(1, &handle);
  return RenderbufferPtr{handle};
}

RenderbufferPtr allocRenderbuffer(GLint width, GLint height, GLenum internalFormat) {
  auto renderbuffer = createRenderbuffer();
  bindRenderbuffer(renderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, width, height);
  unbindRenderbuffer();
  return renderbuffer;
}

void bindRenderbuffer(const RenderbufferPtr& renderbuffer) {
  glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer.get());
}
void unbindRenderbuffer() {
  glBindRenderbuffer(GL_RENDERBUFFER, NULL);
}


ShaderPtr createShader(GLenum type) {
  return ShaderPtr{glCreateShader(type)};
}

ShaderPtr createShaderFromSource(GLenum type, const string& src) {
  auto shader = createShader(type);
  auto length = static_cast<GLint>(src.length());
  auto srcStr = src.c_str();
  glShaderSource(shader.get(), 1, (const char**)&srcStr, &length);
  glCompileShader(shader.get());

#ifdef DEBUG
  GLint shaderOk;
  glGetShaderiv(shader.get(), GL_COMPILE_STATUS, &shaderOk);
  if (!shaderOk) {
    auto log = getInfoLog(shader.get(), &glGetShaderiv, &glGetShaderInfoLog);
    cerr << "Failed to compile shader:\n" << log << endl;
    return {};
  }
#endif

  return shader;
}

ShaderPtr createVertexShaderFromSource(const string& src) {
  return createShaderFromSource(GL_VERTEX_SHADER, src);
}

ShaderPtr createFragmentShaderFromSource(const string& src) {
  return createShaderFromSource(GL_FRAGMENT_SHADER, src);
}


ProgramPtr createProgram() {
  return ProgramPtr{glCreateProgram()};
}

ProgramPtr createProgramFromShaders(GLuint vertexShader, GLuint fragmentShader) {
  auto program = createProgram();
  glAttachShader(program.get(), vertexShader);
  glAttachShader(program.get(), fragmentShader);
  glLinkProgram(program.get());

#ifdef DEBUG
  GLint programOk;
  glGetProgramiv(program.get(), GL_LINK_STATUS, &programOk);
  if (!programOk) {
    auto log = getInfoLog(program.get(), &glGetProgramiv, &glGetProgramInfoLog);
    cerr << "Failed to link shader program:\n" << log << endl;
    return {};
  }
#endif

  return program;
}

ProgramPtr createProgramFromSource(const string& vertexSrc,
                                   const string& fragmentSrc) {
  auto vertexShader = createVertexShaderFromSource(vertexSrc);
  auto fragmentShader = createFragmentShaderFromSource(fragmentSrc);

  if (vertexShader.get() && fragmentShader.get()) {
    return createProgramFromShaders(vertexShader.get(), fragmentShader.get());
  }

  return {};
}

void useProgram(const ProgramPtr& program) {
  glUseProgram(program.get());
}

GLint getUniformLocation(const ProgramPtr& program, const string& name) {
  auto loc = glGetUniformLocation(program.get(), name.c_str());
#ifdef DEBUG
  if (loc < 0) {
    cerr << "No uniform found for name '" << name << "'" << endl;
  }
#endif
  return loc;
}

GLint getAttribLocation(const ProgramPtr& program, const string& name) {
  auto loc = glGetAttribLocation(program.get(), name.c_str());
#ifdef DEBUG
  if (loc < 0) {
    cerr << "No attibute found for name '" << name << "'" << endl;
  }
#endif
  return loc;
}

void setUniform(const ProgramPtr& program, const string& name, GLfloat value) {
  glUniform1f(getUniformLocation(program, name), value);
}
void setUniform(const ProgramPtr& program, const string& name, GLint value) {
  glUniform1i(getUniformLocation(program, name), value);
}

void setUniform(const ProgramPtr& program, const string& name, const Vec4f& value) {
  auto loc = getUniformLocation(program, name);
  glUniform4fv(loc, 1, &value[0]);
}
void setUniform(const ProgramPtr& program, const string& name, const Matrix44f& value) {
  auto loc = getUniformLocation(program, name);
  glUniformMatrix4fv(loc, 1, false, &value[0]);
}


VertexbufferPtr createVertexbuffer() {
  GLuint handle;
  glGenBuffers(1, &handle);
  return VertexbufferPtr{handle};
}

void bindVertexbuffer(const VertexbufferPtr& buffer) {
  glBindBuffer(GL_ARRAY_BUFFER, buffer.get());
}
void unbindVertexbuffer() {
  glBindBuffer(GL_ARRAY_BUFFER, NULL);
}


void setViewport(const ci::Rectf& rect) {
  glViewport(rect.getX1(), rect.getY1(), rect.getWidth(), rect.getHeight());
}

} // namespace gles
