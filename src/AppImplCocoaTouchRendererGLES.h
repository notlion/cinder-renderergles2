#import <UIKit/UIKit.h>

#import "cinder/app/App.h"
#import "RendererGLES.h"

@interface AppImplCocoaTouchRendererGLES : NSObject
{
  cinder::app::App *mApp;
  UIView *mCinderView;
  EAGLContext *mContext;

  EAGLRenderingAPI mApiVersion;

  // The pixel dimensions of the CAEAGLLayer
  GLint mBackingWidth, mBackingHeight;

  // The dimensions of the CAEAGLLayer in points
  GLint mPointsWidth, mPointsHeight;

  // The OpenGL names for the framebuffer and renderbuffer used to render to this view
  GLuint mViewFramebuffer, mViewRenderBuffer, mDepthRenderBuffer;
  GLuint mMsaaFramebuffer, mMsaaRenderBuffer;

  BOOL mUsingMsaa;
  int mMsaaSamples;
}

- (id)initWithFrame:(CGRect)frame
         cinderView:(UIView*)aCinderView
                app:(ci::app::App*)aApp
           renderer:(ci::app::RendererGLES*)aRenderer
     sharedRenderer:(ci::app::RendererGLESRef)sharedRenderer;

- (EAGLContext*)getEaglContext;
- (void)makeCurrentContext;
- (void)flushBuffer;
- (void)setFrameSize:(CGSize)newSize;
- (void)defaultResize;

- (BOOL)needsDrawRect;

@end
