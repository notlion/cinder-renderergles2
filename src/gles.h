#pragma once

#if defined(CINDER_GLES3)
  #include <OpenGLES/ES3/gl.h>
  #include <OpenGLES/ES3/glext.h>
#else
  #include <OpenGLES/ES2/gl.h>
  #include <OpenGLES/ES2/glext.h>
#endif

#include <string>

#include "cinder/Vector.h"
#include "cinder/Matrix.h"
#include "cinder/Rect.h"

namespace gles {

struct TextureDeleter {
  typedef GLuint pointer; // Use a plain GLuint, not *GLuint
  void operator()(GLuint texture) {
    glDeleteTextures(1, &texture);
  }
};
using TexturePtr = std::unique_ptr<GLuint, TextureDeleter>;

struct FramebufferDeleter {
  typedef GLuint pointer;
  void operator()(GLuint framebuffer) {
    glDeleteFramebuffers(1, &framebuffer);
  }
};
using FramebufferPtr = std::unique_ptr<GLuint, FramebufferDeleter>;

struct RenderbufferDeleter {
  typedef GLuint pointer;
  void operator()(GLuint renderbuffer) {
    glDeleteRenderbuffers(1, &renderbuffer);
  }
};
using RenderbufferPtr = std::unique_ptr<GLuint, RenderbufferDeleter>;

struct ShaderDeleter {
  typedef GLuint pointer;
  void operator()(GLuint shader) {
    glDeleteShader(shader);
  }
};
using ShaderPtr = std::unique_ptr<GLuint, ShaderDeleter>;

struct ProgramDeleter {
  typedef GLuint pointer;
  void operator()(GLuint program) {
    glDeleteProgram(program);
  }
};
using ProgramPtr = std::unique_ptr<GLuint, ProgramDeleter>;

struct VertexbufferDeleter {
  typedef GLuint pointer;
  void operator()(GLuint buffer) {
    glDeleteBuffers(1, &buffer);
  }
};
using VertexbufferPtr = std::unique_ptr<GLuint, VertexbufferDeleter>;


// Texture

struct TexturePixelFormat {
  GLint  internalFormat = GL_RGBA;
  GLenum format         = GL_RGBA;
  GLenum type           = GL_UNSIGNED_BYTE;
};

TexturePtr createTexture();
TexturePtr allocTexture2d(GLsizei width, GLsizei height, TexturePixelFormat fmt = TexturePixelFormat{});

void bindTexture2d(const TexturePtr& texture);
void bindTexture2d(const TexturePtr& texture, GLenum unit);
void unbindTexture2d();

void setTextureFilterMin(GLint filter);
void setTextureFilterMag(GLint filter);
void setTextureFilter(GLint filter);

void setTextureWrapS(GLint wrap);
void setTextureWrapT(GLint wrap);
void setTextureWrap(GLint wrap);


// Frambuffer

FramebufferPtr createFramebuffer();

void bindFramebuffer(const FramebufferPtr& framebuffer);
void unbindFramebuffer();

bool checkFramebufferComplete();


// Renderbuffer

RenderbufferPtr createRenderbuffer();
RenderbufferPtr allocRenderbuffer(GLint width, GLint height, GLenum internalFormat = GL_DEPTH_COMPONENT16);

void bindRenderbuffer(const RenderbufferPtr& renderbuffer);
void unbindRenderbuffer();


// Shader and Shader Program

ShaderPtr createShader(GLenum type);
ShaderPtr createShaderFromSource(GLenum type, const std::string& src);
ShaderPtr createVertexShaderFromSource(const std::string& src);
ShaderPtr createFragmentShaderFromSource(const std::string& src);

ProgramPtr createProgram();
ProgramPtr createProgramFromShaders(GLuint vertexShader, GLuint fragmentShader);
ProgramPtr createProgramFromSource(const std::string& vertexSrc,
                                   const std::string& fragmentSrc);

void useProgram(const ProgramPtr& program);

GLint getUniformLocation(const ProgramPtr& program, const std::string& name);
GLint getAttribLocation(const ProgramPtr& program, const std::string& name);

void setUniform(const ProgramPtr& program, const std::string& name, GLfloat value);
void setUniform(const ProgramPtr& program, const std::string& name, GLint value);

void setUniform(const ProgramPtr& program, const std::string& name, const ci::Vec4f& value);
void setUniform(const ProgramPtr& program, const std::string& name, const ci::Matrix44f& value);


// Vertex Buffer

VertexbufferPtr createVertexbuffer();

void bindVertexbuffer(const VertexbufferPtr& buffer);
void unbindVertexbuffer();

template<typename T>
VertexbufferPtr createVertexbufferWithArray(const std::vector<T>& array,
                                            GLenum usage = GL_STATIC_DRAW) {
  auto buffer = createVertexbuffer();
  bindVertexbuffer(buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(T) * array.size(), &array[0], usage);
  return buffer;
}


// Misc Helpers

void setViewport(const ci::Rectf& rect);

} // namespace gles
