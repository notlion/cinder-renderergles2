#pragma once

#include "cinder/Area.h"
#include "cinder/app/Renderer.h"

#if defined __OBJC__
  @class AppImplCocoaTouchRendererGLES;
#else
  class AppImplCocoaTouchRendererGLES;
#endif

namespace cinder { namespace app {

typedef std::shared_ptr<class RendererGLES> RendererGLESRef;

class RendererGLES : public Renderer {
public:
  enum ApiVersion {
    API_GLES2,
    API_GLES3
  };

  enum {
    AA_NONE = 0,
    AA_MSAA_2,
    AA_MSAA_4,
    AA_MSAA_6,
    AA_MSAA_8,
    AA_MSAA_16,
    AA_MSAA_32
  };
  static const int sAntiAliasingSamples[];

  RendererGLES(ApiVersion aApiVersion = API_GLES2, int aAntiAliasing = AA_MSAA_4);
  ~RendererGLES() {}

//  static RendererGLESRef create(int antiAliasing = AA_MSAA_4) {
//    return std::make_shared<RendererGLES>( antiAliasing);
//  }
  virtual RendererRef clone() const {
    return std::make_shared<RendererGLES>(*this);
  }

  virtual void setup(App *aApp, const Area &frame, UIView *cinderView, RendererRef sharedRenderer);
  virtual bool isEaglLayer() const { return true; }
  virtual void setFrameSize(int width, int height);

  EAGLContext* getEaglContext() const;

  ApiVersion getApiVersion() const { return mApiVersion; }

  void setAntiAliasing(int aAntiAliasing);
  int  getAntiAliasing() const { return mAntiAliasing; }

  virtual void startDraw();
  virtual void finishDraw();
  virtual void defaultResize();
  virtual void makeCurrentContext();
  virtual Surface copyWindowSurface(const Area &area);

protected:
  RendererGLES(const RendererGl &renderer);

  ApiVersion mApiVersion;
  int mAntiAliasing;
  AppImplCocoaTouchRendererGLES *mImpl;
};

}} // namespace cinder::app
